const { exec } = require("child_process");
var a = [{deploymentName:"nodeapp", image:"node-app", containerName:"nodeapp"}]

var processed_Deployment = [];


var IMAGETAG = process.env.IMAGETAG;
console.log(process.env.IMAGETAG);

startDeployment();

async function startDeployment(argument) {
	let isRollback = false;
	for(let depName of a){	
		let isSuccess = await executeDeploymentCommand(depName);
		if (!isSuccess) {
			isRollback = true;
		    await rollbackDeployment();
			break;
		}
	}


	if (isRollback) {
		console.log('Exit with rollback');
		process.exit(1);
	} else {
		console.log('Exit with success');
		process.exit(0);
	}
}

async function executeDeploymentCommand(depName){
	return new Promise((resolve)=>{
		exec(`kubectl set image deployment/${depName.deploymentName} ${depName.containerName}=gcr.io/sample-project-266811/${depName.image}:${IMAGETAG} && kubectl rollout status deployment/${depName.deploymentName}`, (error, stdout, stderr) => {
		    processed_Deployment.push(depName.deploymentName); 
			if (error) {
				console.log(`stdout for error : ${stderr}`);
		    	resolve(false);
		    } else {
			    console.log(`stdout for image update : ${stdout}`);
		    	resolve(true);	
		    }
		});
	});
} 

async function rollbackDeployment() {
	console.log('processed_Deployment',processed_Deployment)
	if (processed_Deployment && processed_Deployment.length) {
		for(let element of processed_Deployment.reverse()){	
			await executeRollbackCommand(element);
		}	
	}
}

async function executeRollbackCommand(element){
	return new Promise((resolve)=>{
		console.log('executeRollbackCommand: ', element);
		exec(`kubectl rollout undo deployment/${element} && kubectl rollout status deployment ${element}`, (error, stdout, stderr) => {
	       	if(error) {
	       		console.log('Rollback error:', error);
				resolve(false);
	       	} else {
				console.log(`Rollback stdout for status: ${stdout}`);
	       		resolve(true);	       		
	       	}
		});
	});
}