const { exec } = require("child_process");
var a = [{deploymentName:"my-nginx", image:"nginx", containerName:"my-nginx"}, {deploymentName:"nodeapp", image:"nodejs-image-demo", containerName:"nodeapp"}, {deploymentName:"nginx2", image:"nginx", containerName:"nginx2"}]
// var all_deployments = [ 'my-nginx', 'nodeapp', 'nginx2' ];
var processed_Deployment = [];
// var image_name = ['nginx', 'nodejs-image-demo', 'nginx'];

var IMAGETAG = process.env.IMAGETAG;
console.log(process.env.IMAGETAG);

startDeployment();

async function startDeployment(argument) {
	let isRollback = false;
	for(let depName of a){	
		let isSuccess = await executeDeploymentCommand(depName);
		if (!isSuccess) {
			isRollback = true;
		    await rollbackDeployment();
			break;
		}
	}


	if (isRollback) {
		console.log('Exit with rollback');
		process.exit(1);
	} else {
		console.log('Exit with success');
		process.exit(0);
	}
}

async function executeDeploymentCommand(depName){
	return new Promise((resolve)=>{
		exec(`kubectl set image deployment/${depName.deploymentName} ${depName.containerName}=miteshgangaramani/${depName.image}:${IMAGETAG} && kubectl rollout status deployment/${depName.deploymentName}`, (error, stdout, stderr) => {
		    processed_Deployment.push(depName.deploymentName); 
			if (error) {
				console.log(`stdout for error : ${stderr}`);
		    	resolve(false);
		    } else {
			    console.log(`stdout for image update : ${stdout}`);
		    	resolve(true);	
		    }
		});
	});
} 

async function rollbackDeployment() {
	console.log('processed_Deployment',processed_Deployment)
	if (processed_Deployment && processed_Deployment.length) {
		for(let element of processed_Deployment.reverse()){	
			await executeRollbackCommand(element);
		}	
	}
}

async function executeRollbackCommand(element){
	return new Promise((resolve)=>{
		console.log('executeRollbackCommand: ', element);
		exec(`kubectl rollout undo deployment/${element} && kubectl rollout status deployment ${element}`, (error, stdout, stderr) => {
	       	if(error) {
	       		console.log('Rollback error:', error);
				resolve(false);
	       	} else {
				console.log(`Rollback stdout for status: ${stdout}`);
	       		resolve(true);	       		
	       	}
		});
	});
}